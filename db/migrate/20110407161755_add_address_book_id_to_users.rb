class AddAddressBookIdToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :address_book_id, :integer
  end

  def self.down
    remove_column :users, :address_book_id
  end
end
