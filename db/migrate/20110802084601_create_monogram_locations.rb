class CreateMonogramLocations < ActiveRecord::Migration
  def self.up
    create_table :monogram_locations do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :monogram_locations
  end
end
