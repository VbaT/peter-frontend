class AddOrderMeasurement < ActiveRecord::Migration
  def self.up
    add_column :orders, :measurement_field, :text
    add_column :orders, :address_field, :text    
  end

  def self.down
    remove_column :orders, :measurement_field      
    remove_column :orders, :address_field          
  end
end
