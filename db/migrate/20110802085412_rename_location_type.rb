class RenameLocationType < ActiveRecord::Migration
  def self.up
    rename_column :locations, :location_type, :type
  end

  def self.down
    rename_column :locations, :type, :location_type    
  end
end
