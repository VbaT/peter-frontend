class CreateFabricTypes < ActiveRecord::Migration
  def self.up
    create_table :fabric_types do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :fabric_types
  end
end
