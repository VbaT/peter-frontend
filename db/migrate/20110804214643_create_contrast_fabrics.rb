class CreateContrastFabrics < ActiveRecord::Migration
  def self.up
    create_table :contrast_fabrics do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :contrast_fabrics
  end
end
