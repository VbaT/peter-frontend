class AddGenderToFeatureCategory < ActiveRecord::Migration
  def self.up
    add_column :feature_categories, :gender, :string
  end

  def self.down
    remove_column :feature_categories, :gender
  end
end
