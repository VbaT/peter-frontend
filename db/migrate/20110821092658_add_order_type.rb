class AddOrderType < ActiveRecord::Migration
  def self.up  
    add_column :orders, :cloth_type, :string, :default => "shirt"
  end

  def self.down
    remove_column :orders, :cloth_type
  end
end
