class AddTypeToFabric < ActiveRecord::Migration
  def self.up
    add_column :fabrics, :type, :string, :default => "ShirtFabric"
  end

  def self.down
    remove_column :fabrics, :type
  end
end
