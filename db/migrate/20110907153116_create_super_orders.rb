class CreateSuperOrders < ActiveRecord::Migration
  def self.up
    create_table :super_orders do |t|
      t.string  :name
      t.integer :user_id
      t.string  :order_id_field
      t.string  :address_field

      t.timestamps
    end
  end

  def self.down
    drop_table :super_orders
  end
end
