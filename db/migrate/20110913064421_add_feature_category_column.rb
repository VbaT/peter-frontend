class AddFeatureCategoryColumn < ActiveRecord::Migration
  def self.up
    add_column :feature_categories, :cloth_type, :string
    add_column :feature_categories, :translate, :string
    remove_column :feature_categories, :gender
  end

  def self.down
    remove_column :feature_categories, :cloth_type
    remove_column :feature_categories, :translate
    add_column :feature_categories, :gender, :string    
  end
end
