class CreateJacketFabrics < ActiveRecord::Migration
  def self.up
    create_table :jacket_fabrics do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :jacket_fabrics
  end
end
