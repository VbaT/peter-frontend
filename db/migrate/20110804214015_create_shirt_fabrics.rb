class CreateShirtFabrics < ActiveRecord::Migration
  def self.up
    create_table :shirt_fabrics do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :shirt_fabrics
  end
end
