class CreateTrouserFabrics < ActiveRecord::Migration
  def self.up
    create_table :trouser_fabrics do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :trouser_fabrics
  end
end
