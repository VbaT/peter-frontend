class AddFeatureColumn < ActiveRecord::Migration
  def self.up
    add_column :features, :translate, :string
    add_column :features, :gender, :string
    add_column :features, :cloth_type, :string
  end

  def self.down
    remove_column :features, :translate
    remove_column :features, :gender    
    remove_column :features, :cloth_type
  end
end
