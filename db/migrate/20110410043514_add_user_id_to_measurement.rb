class AddUserIdToMeasurement < ActiveRecord::Migration
  def self.up
    add_column :measurements, :user_id, :integer
  end

  def self.down
    remove_column :measurements, :user_id
  end
end
