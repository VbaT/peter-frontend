class AddPrice < ActiveRecord::Migration
  def self.up
    add_column :button_colors, :price, :number, :default => 0
    add_column :locations, :price, :number, :default => 0    
    add_column :thread_colors, :price, :number, :default => 0
    
  end

  def self.down
    remove_column :button_colors, :price
    remove_column :locations, :price
    remove_column :thread_colors, :price
  end
end
