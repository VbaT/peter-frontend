class AddNameToMeasurement < ActiveRecord::Migration
  def self.up
    add_column :measurements, :name, :string
  end

  def self.down
    remove_column :measurements, :name
  end
end