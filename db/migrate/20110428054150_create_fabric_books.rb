class CreateFabricBooks < ActiveRecord::Migration
  def self.up
    create_table :fabric_books do |t|
      t.string :fabric_book_code

      t.timestamps
    end
  end

  def self.down
    drop_table :fabric_books
  end
end
