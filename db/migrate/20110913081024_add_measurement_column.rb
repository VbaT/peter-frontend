class AddMeasurementColumn < ActiveRecord::Migration
  def self.up
    add_column :measurements, :length, :float    
    add_column :measurements, :vest, :float
    add_column :measurements, :upper_hip, :float    
    add_column :measurements, :p_waist, :float
    add_column :measurements, :p_hips, :float
    add_column :measurements, :crotch, :float    
    add_column :measurements, :thighs, :float
    add_column :measurements, :knee, :float
    add_column :measurements, :bottom, :float    
    add_column :measurements, :p_length, :float    
    add_column :measurements, :inseam, :string
    add_column :measurements, :shorts, :string        
    add_column :measurements, :body_part, :string    
    rename_column :measurements, :back_armhole, :back_piece
    rename_column :measurements, :front_armhole, :front_piece    
    rename_column :measurements, :shirt_length, :short_sleeves
    rename_column :measurements, :armhole_circum, :armhole
    rename_column :measurements, :bicep_circum, :bicep    
    rename_column :measurements, :wrist_circum, :cuff
    rename_column :measurements, :waist_circum, :waist
    rename_column :measurements, :hip_circum, :hips
    rename_column :measurements, :sleeve_length, :sleeves
    rename_column :measurements, :chest_circum, :chest 
    rename_column :measurements, :shoulder_width, :shoulders    
    rename_column :measurements, :neck_circum, :neck
    
  end

  def self.down                      
    remove_column :measurements, :length
    remove_column :measurements, :vest
    remove_column :measurements, :upper_hip
    remove_column :measurements, :p_waist
    remove_column :measurements, :p_hips
    remove_column :measurements, :crotch
    remove_column :measurements, :thighs
    remove_column :measurements, :knee
    remove_column :measurements, :bottom
    remove_column :measurements, :p_length
    remove_column :measurements, :inseam
    remove_column :measurements, :shorts
    remove_column :measurements, :body_part
    rename_column :measurements, :back_piece, :back_armhole 
    rename_column :measurements, :front_piece, :front_armhole    
    rename_column :measurements, :short_sleeves, :shirt_length 
    rename_column :measurements, :armhole, :armhole_circum
    rename_column :measurements, :bicep, :bicep_circum    
    rename_column :measurements, :cuff, :wrist_circum
    rename_column :measurements, :waist, :waist_circum
    rename_column :measurements, :hips, :hip_circum
    rename_column :measurements, :sleeves, :sleeve_length    
    rename_column :measurements, :chest, :chest_circum 
    rename_column :measurements, :shoulders, :shoulder_width    
    rename_column :measurements, :neck, :neck_circum
  end
end
