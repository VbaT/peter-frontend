class AddMetaFieldsToFabrics < ActiveRecord::Migration
  def self.up
    add_column :fabrics, :fabric_book_id, :integer
    add_column :fabrics, :fabric_code, :string
    add_column :fabrics, :fabric_no, :integer
    remove_column :fabrics, :fabric_number
  end

  def self.down
    add_column :fabrics, :fabric_number, :string
    remove_column :fabrics, :fabric_book_id
    remove_column :fabrics, :fabric_code
    remove_column :fabrics, :fabric_book
  end
end