class AddPriceToFeatures < ActiveRecord::Migration
  def self.up
    add_column :features, :price, :number
  end

  def self.down
    remove_column :features, :price
  end
end