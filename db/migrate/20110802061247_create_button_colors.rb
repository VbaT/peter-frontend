class CreateButtonColors < ActiveRecord::Migration
  def self.up
    create_table :button_colors do |t|
      t.integer :button_color_id
      t.string :name
      t.string :photo_file_name
      t.integer :photo_file_size
      t.string :photo_file_type

      t.timestamps
    end
  end

  def self.down
    drop_table :button_colors
  end
end
