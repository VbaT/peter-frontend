class AddAddressname < ActiveRecord::Migration
  def self.up
    add_column :address_books, :name, :string, :default => "unknown"
  end

  def self.down
    remove_column :address_books, :name
  end
end
