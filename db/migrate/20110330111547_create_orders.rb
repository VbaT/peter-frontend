class CreateOrders < ActiveRecord::Migration
  def self.up
    create_table :orders do |t|
      t.integer :fabric_id
      t.integer :measurement_id
      t.text :feature_field

      t.timestamps
    end
  end

  def self.down
    drop_table :orders
  end
end
