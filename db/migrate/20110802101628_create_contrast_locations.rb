class CreateContrastLocations < ActiveRecord::Migration
  def self.up
    create_table :contrast_locations do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :contrast_locations
  end
end
