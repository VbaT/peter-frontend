class AddVolumeToOrder < ActiveRecord::Migration
  def self.up  
    add_column :orders, :volume, :integer, :default => 1
    add_column :orders, :price, :number, :default => 0    
  end

  def self.down
    remove_column :orders, :volume
    remove_column :orders, :price    
  end
end
