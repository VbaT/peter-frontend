class CreateThreadColors < ActiveRecord::Migration
  def self.up
    create_table :thread_colors do |t|
      t.string :name
      t.string :photo_file_name
      t.string :photo_file_type
      t.integer :photo_file_size

      t.timestamps
    end
  end

  def self.down
    drop_table :thread_colors
  end
end
