class AddFabricTypeIdToFabrics < ActiveRecord::Migration
  def self.up
    add_column :fabrics, :fabric_type_id, :integer   
  end

  def self.down
    remove_column :fabrics, :fabric_type_id
  end
end
