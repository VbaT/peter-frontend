class ModifyOrder < ActiveRecord::Migration
  def self.up
    remove_column :orders, :address_id
    remove_column :orders, :address_field
    remove_column :orders, :measurement_id        
    add_column    :orders, :order_name, :text
  end

  def self.down
    add_column :orders, :address_id, :integer
    add_column :orders, :measurement_id, :integer    
    add_column :orders, :address_field, :text
    remove_column :orders, :order_name
  end
end
