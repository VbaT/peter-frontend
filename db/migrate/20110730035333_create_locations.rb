class CreateLocations < ActiveRecord::Migration
  def self.up
    create_table :locations do |t|
      t.integer :location_id
      t.string :name
      t.string :location_type
      t.string :photo_file_name
      t.integer :photo_file_size
      t.string :photo_file_type

      t.timestamps
    end
  end

  def self.down
    drop_table :locations
  end
end
