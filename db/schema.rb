# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110913081024) do

  create_table "address_books", :force => true do |t|
    t.string   "address"
    t.string   "city"
    t.string   "country"
    t.integer  "postcode"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "name",       :default => "unknown"
  end

  create_table "admins", :force => true do |t|
    t.string   "email",                               :default => "", :null => false
    t.string   "encrypted_password",   :limit => 128, :default => "", :null => false
    t.string   "password_salt",                       :default => "", :null => false
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "body_shapes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "button_colors", :force => true do |t|
    t.integer  "button_color_id"
    t.string   "name"
    t.string   "photo_file_name"
    t.integer  "photo_file_size"
    t.string   "photo_file_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price",           :default => 0.0
  end

  create_table "chests", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "colors", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contrast_fabrics", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contrast_locations", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fabric_books", :force => true do |t|
    t.string   "fabric_book_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fabric_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fabrics", :force => true do |t|
    t.string   "name"
    t.integer  "color_id"
    t.decimal  "price"
    t.integer  "style_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fabric_book_id"
    t.string   "fabric_code"
    t.integer  "fabric_no"
    t.integer  "fabric_type_id"
    t.string   "type",               :default => "ShirtFabric"
  end

  create_table "feature_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cloth_type"
    t.string   "translate"
  end

  create_table "features", :force => true do |t|
    t.string   "name"
    t.integer  "feature_category_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price"
    t.string   "translate"
    t.string   "gender"
    t.string   "cloth_type"
  end

  create_table "jacket_fabrics", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", :force => true do |t|
    t.integer  "location_id"
    t.string   "name"
    t.string   "type"
    t.string   "photo_file_name"
    t.integer  "photo_file_size"
    t.string   "photo_file_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price",           :default => 0.0
  end

  create_table "measurements", :force => true do |t|
    t.float    "neck"
    t.float    "shoulders"
    t.float    "chest",         :limit => 255
    t.float    "front_piece"
    t.float    "back_piece"
    t.float    "waist"
    t.float    "hips"
    t.float    "short_sleeves"
    t.float    "sleeves"
    t.float    "armhole"
    t.float    "bicep"
    t.float    "cuff"
    t.float    "height_feet"
    t.float    "height_inches"
    t.float    "weight_lb"
    t.string   "shoulder"
    t.string   "body_shape"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "name"
    t.float    "length"
    t.float    "vest"
    t.float    "upper_hip"
    t.float    "p_waist"
    t.float    "p_hips"
    t.float    "crotch"
    t.float    "thighs"
    t.float    "knee"
    t.float    "bottom"
    t.float    "p_length"
    t.string   "inseam"
    t.string   "shorts"
    t.string   "body_part"
  end

  create_table "monogram_locations", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", :force => true do |t|
    t.integer  "fabric_id"
    t.text     "feature_field"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "volume",            :default => 1
    t.decimal  "price",             :default => 0.0
    t.string   "cloth_type",        :default => "shirt"
    t.text     "measurement_field"
    t.text     "order_name"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "shirt_fabrics", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shoulders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "styles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "super_orders", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.string   "order_id_field"
    t.string   "address_field"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "thread_colors", :force => true do |t|
    t.string   "name"
    t.string   "photo_file_name"
    t.string   "photo_file_type"
    t.integer  "photo_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price",           :default => 0.0
  end

  create_table "trouser_fabrics", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                               :default => "",         :null => false
    t.string   "encrypted_password",   :limit => 128, :default => "",         :null => false
    t.string   "password_salt",                       :default => "",         :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "address_book_id"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "gender"
    t.string   "user_type",                           :default => "reseller"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
