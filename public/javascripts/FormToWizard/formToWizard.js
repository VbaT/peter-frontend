/* Created by jankoatwarpspeed.com */

(function($) {

    $.fn.formToWizard = function(options) {
        options = $.extend({  
            submitButton: "" 
        }, options); 
        
        var element = this;
        var steps = $(element).find("fieldset");
        var count = steps.size();
        var submmitButtonName = "#" + options.submitButton;
				var currentStep = 0;
        $(submmitButtonName).hide();
        // 2
        $(element).before("<ul id='steps'></ul>");

        steps.each(function(i) {
            $(this).wrap("<div id='step" + i + "'></div>");
            $(this).append("<p id='step" + i + "commands'></p>");
            var name = $(this).find("legend").html();
            $("#steps").append("<li id='stepDesc" + i + "' class='selected'>" + "<div id='menu"+ name + "'>" + "<em>" + i + "</em>" + "<span>" + name + "</span></div></li>");
			$('#stepDesc' + i).bind("click", function(e) {
	            var stepName = "step" + (currentStep);
				$("#" + stepName).hide();
                $("#step" + (i)).show();
                if ((i + 1) == count)
                    $(submmitButtonName).show();
				else
					$(submmitButtonName).hide();
				selectStep(i)
            });

            if (i == 0) {
                // createNextButton(i);
                selectStep(i);
            }
            else if (i == count - 1) {
                $("#step" + i).hide();
                // createPrevButton(i);
            }
            else {
                $("#step" + i).hide();
                // createPrevButton(i);
                // createNextButton(i);
            }
        });

        function createPrevButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev'>< Back</a>");

            $("#" + stepName + "Prev").bind("click", function(e) {
                $("#" + stepName).hide();
                $("#step" + (currentStep - 1)).show();
                $(submmitButtonName).hide();
                selectStep(currentStep - 1);
            });
        }

        function createNextButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='next'>Next ></a>");
			
            $("#" + stepName + "Next").bind("click", function(e) {
                $("#" + stepName).hide();
                $("#step" + (currentStep + 1)).show();
                if (currentStep + 2 == count)
                    $(submmitButtonName).show();
                selectStep(currentStep + 1);
            });
        }

        function selectStep(i) {
			$("#stepDesc" + currentStep).removeClass("current");
			currentStep = i;
			$("#stepDesc" + i).addClass("current");
        }

    }
})(jQuery);
