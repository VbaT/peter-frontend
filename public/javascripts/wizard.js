$(document).ready(function() {
    $("#new_measurement").validate();	
    applyStatus();    
    $(".disabled a").live("click", function(){
      return false;
    });
});

function delete_item(input){
	if(typeof(input) == "string"){
		input = {item: input};
	}else{
		input = $(input).serializeArray();
	}

	$.post("/cart/delete_item", input, function(){
		window.location.reload();
	});        
}

function init_fabrics(){
  var page = 	$("#page").attr("value");
  var style = $("#styles_collection").attr("value");
  var color = $("#colors_collection").attr("value");
  $.get("/wizard/get_fabrics?page=" + page + '&style[name]=' + style + '&color[name]=' + color, function(data){
    $("#choices").html(data);
  });
}


function init_ajax_fabrics(){
  $("input").change(function(){   
    $(".highlighted").removeClass("highlighted");
    $(this).parent().addClass("highlighted"); 
    $.post("/wizard/save_fabric", {fabric:this.value, fabric_page:$("#page").attr("value")}, function(){
      applyStatus();
    });
  });

  $(".pagination a").live("click", function() {
    $(".pagination").html("<span>Page is loading...</span>");
    var style = $("#styles_collection").attr("value");
    var color = $("#colors_collection").attr("value");
    $.get(this.href + '&style[name]=' + style + '&color[name]=' + color, function(data){
      $("#choices").html(data);
      $("#page").attr("value", $('span.current').html())
    });
    return false;
  });
}

function init_features(){  
  $("#CreateForm").formToWizard({submitButton: "CreateShirt" });  
	$("#CreateForm fieldset.invalid").each(function(){
		var stepId = $(this).attr("id");
		$("#menu" + stepId).parent().removeClass("selected");
	});
  $("#CreateForm div").each(function(index){     
    if($(this).find(".highlighted").hasClass("highlighted")){
      $(this).addClass("selected");
      // var currentStepId = $(this).attr("id");
      // currentStepId = "stepDesc" + currentStepId.charAt(currentStepId.length-1);
      // $("#" + currentStepId).addClass("selected");
    }
  });     
	init_monogram();                    
	init_contrast();
  $("#CreateForm input").click(function(){
		if($(this).attr("id") == "text_monogram"){
			return;
		}
		$(this).parents("ul").children(".highlighted").removeClass("highlighted");  		
		$(this).parent().addClass("highlighted");
    var currentStepId = $(this).parents("fieldset").parent().attr("id");
    currentStepId = "stepDesc" + currentStepId.charAt(currentStepId.length-1)
    $("#" + currentStepId).addClass("selected"); 
    $.post("/wizard/save_feature", $("#CreateForm").serializeArray(), function(data){ 
      applyStatus();
    });
  }); 
}   

function init_monogram(){                                          	
	if(!$("#CreateForm ul.monogram > li").hasClass(".highlighted")){
		$("#monogramBlock").hide();                                          
	}
	if($("#CreateForm ul.monogram > li.highlighted").hasClass(".noFeatureUsed")){
		$("#monogramBlock").hide();                                          
	}
	$("#text_monogram").change(function(){
		$.post("/wizard/save_feature", $("#CreateForm").serializeArray(), function(data){
      applyStatus();
    });
	})
	$("#CreateForm ul.monogram input").click(function(){
		if($(this).parent().attr("class") == "noFeatureUsed"){  
			$("#monogramBlock").hide("fast");                                          
			$("#monogramBlock ul").children(".highlighted").find("input").removeAttr("checked");			
			$("#monogramBlock ul").children(".highlighted").removeClass("highlighted");
			$("#text_monogram").val("");
			$.post("/wizard/delete_monogram_info", {name:$(this).attr("name")});
		}else{
			$("#monogramBlock").show("fast");
		}
	});	 
}
function init_contrast(){                                          	
	if(!$("#CreateForm ul.contrast > li").hasClass(".highlighted")){
		$("#contrastBlock").hide();                                          
	}	
	if($("#CreateForm ul.contrast > li.highlighted").hasClass(".noFeatureUsed")){
		$("#contrastBlock").hide();
	}
	$("#CreateForm ul.contrast input").click(function(){
		if($(this).parent().attr("class") == "noFeatureUsed"){  
			$("#contrastBlock").hide("fast");                                          
			$("#contrastBlock ul").children(".highlighted").find("input").removeAttr("checked");
			$("#contrastBlock ul").children(".highlighted").removeClass("highlighted");
			$.post("/wizard/delete_monogram_info", $(this).attr("name"));
		}else{
			$("#contrastBlock").show("fast");
		}
	});	 
}

function init_measurements(){
  $("#selection ul").each(function(index){
		console.log($(this));
    $(this).children().each(function(){
      $(this).find('input[checked="true"]').parent().addClass("highlighted");
    });
    $(this).children().click(function(){
      $(this).find("input").attr("checked", "true");
      $(this).parent().children("li").removeClass("highlighted");
      $(this).addClass("highlighted");
    });
    $("#new_measurement").validate();
  });
	init_quantity();                
	init_select_measurement()	
}
 
function init_select_measurement(){
	$("#measurement_user_id").change(function(){
		$.getJSON("/wizard/get_measurement", {measurement_id: $(this).val()}, function(data){
			var measurement = data.measurement
			$.each(measurement, function(key, value){
				var name = $("#measurement_" + key)
				if(name.is("input")){
					name.attr("value", value);
				}else{
					var figure_name = $("#measurement_" + key + "_" + value);
					figure_name.attr("checked", "true");
      		figure_name.parent().parent().children("li").removeClass("highlighted");					
					figure_name.parent().addClass("highlighted");
				}
			});
		});
	});
}

function init_info(){
	$("#address_book_user_id").change(function(){
    $.getJSON("/wizard/save_address", {address_id: $(this).val(), order_name: $("#post_title").val()}, function(data){
			var address = data.address_book
			$.each(address, function(key, value){
				var name = $("#address_" + key);
				name.html(value);
			})
    });		
	});
}   

function init_quantity(unit_price){
	$("#quantity").change(function(){
		var total_price = unit_price * $(this).attr("value") 
		$("#inline_quantity").html($(this).attr("value"));		
		$.post("/wizard/save_quantity", {quantity:$(this).attr("value")}, function(data){
			$("#unit_price").html(unit_price);			
			$("#total_price").html(total_price);
    });
		// $(this).attr("value", )
	});
} 

function save_measurement(){                                                                   	
  if($("#new_measurement").valid()){
    $.post("/wizard/save_measurement", $("#new_measurement").serializeArray(), function(data){
      applyStatus();
			window.location = "/wizard/summary";
    });
  }
}

function save_summary(){
    $.post("/wizard/save_summary", {quantity:$("#quantity").attr("value")}, function(data){
      applyStatus();
      window.location = "/wizard/confirm"
    });
}

function applyStatus(){	
  $.getJSON("/wizard/states", function(data){	 	
		if((data.invalid.length != 0) || (data.measurement == true)){
			$("#steps li").each(function(){
				$(this).addClass("selected");
			});	     
		}
		$.each(data.invalid, function(index, value){
			$("#menu" + value).parent().removeClass("selected");
		});
    $.each(data, function(index, value){	
      var list = $('#' + index);
      value ? list.removeClass("disabled") : list.addClass("disabled");
    })
  })
}


function nextSection(){
  var url = $("#guide li.active").next().find("a")[0].href;
  window.location = url;
}