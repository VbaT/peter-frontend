ActionController::Routing::Routes.draw do |map|
  map.resources :thread_colors
  map.resources :button_colors
  map.resources :locations 
  map.resources :monogram_locations
  map.resources :contrast_locations  
  map.resources :fabric_types
  map.resources :fabric_books  
  map.resources :shirt_fabrics
  map.resources :contrast_fabrics  
  map.resources :address_books
  map.devise_for :users, :controllers => {:registrations => "registrations"}        
  # map.connet '/orders/show_order', :controller => 'orders', :action => 'show_order'
  map.connet '/orders/show_order/:id', :controller => 'orders', :action => 'show_order'
  map.connet '/orders/print_super_order/:id', :controller => 'orders', :action => 'print_super_order'
  map.connet '/orders/print_order/:id', :controller => 'orders', :action => 'print_order'
  
  
  map.resources :orders
  map.resources :body_shapes
  map.resources :measurements
  map.resources :chests
  map.resources :shoulders
  map.devise_for :admins
  map.resources :feature_categories
  map.resources :features
  map.resources :colors
  map.resources :styles
  map.resources :fabrics

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view' 
  map.connect 'admin', :controller => 'fabrics'
  map.connect '/wizard/summary', :controller => 'wizard', :action => 'summary'
  map.connect '/account/measurement_new', :controller => 'account', :action => 'measurement_new'
  map.connect '/wizard/get_fabrics', :controller => 'wizard', :action => 'get_fabrics'
  map.connect '/about', :controller => 'page', :action => 'about' 
  map.connect '/contact', :controller => 'page', :action => 'contact'
  map.connect '/shopping', :controller => 'wizard', :action => 'index'
  map.connect '/barcode/:id.:format', :controller => "barcode", :action => "get_barcode_image"
  # Keep in mind you can assign values other than :controller and :action
  
  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  

  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  #map.root :controller => "wizard"
  map.root :controller => 'page'
  map.connect ':controller/:action.:format'
  map.connect ':controller/:action'
  
  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
