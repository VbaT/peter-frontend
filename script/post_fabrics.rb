#! /usr/bin/ruby
require 'rubygems'
require 'httpclient'

def create_new_fabric(post_url, name, fabric_book, fabric_code, fabric_no, file_path = nil)
  post_data = {
    :"fabric[name]" => name,
    :"fabric[color_id]" => 14,
    :"fabric[style_id]" => 9,
    :"fabric[price]" => 0,
    :"fabric_book" => fabric_book,
    :"fabric[fabric_code]" => fabric_code,
    :"fabric[fabric_no]" => fabric_no,
    }
  if file_path
    post_data[:"fabric[photo]"] = File.new(file_path)
  end

  HTTPClient.post post_url, post_data
end

# puts create_new_fabric("http://localhost:4000/fabrics/create",
#                   "name1",
#                   "book1",
#                   "code_111",
#                   12,
#                   "/Users/x/Desktop/1.png"
# ).body.inspect


PATH = "/Users/x/Desktop/fabrics"
JUNK = [".", "..",".DS_Store"]
POST_URL = "http://localhost:4000/fabrics/create"
books = Dir.entries(PATH) - JUNK
books.each do |book|
  (Dir.entries(PATH + "/" + book) - JUNK).each do |file_name|
    matched_data = file_name.match(/([0-9]+)_([0-9a-z]+)\-([0-9a-z]+)/).inspect
    puts "filename #{file_name}"
    fabric_book = book
    fabric_code = "#{$2}-#{$3}"
    fabric_no = "#{$1}"
    puts 
    create_new_fabric(POST_URL, file_name, fabric_book, fabric_code, fabric_no, PATH + "/" + book + "/" + file_name)
  end
end
  
  
=begin
  # SAMPLE CODE
  require "post_data"
  puts create_new_episode("http://www.designgonemad.com/episodes", "channel_name", channel_no, "description", channel_id )

  create_table "fabrics", :force => true do |t|
    t.string   "name"
    t.integer  "color_id"
    t.decimal  "price"
    t.integer  "style_id"
    t.integer  "fabric_book_id"
    t.string   "fabric_code"
    t.integer  "fabric_no"
  end
=end