# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  
  def edit_image
    image_tag("/images/edit.png",:alt=>'edit',:title=>'edit')
  end
  def delete_image
    image_tag("/images/delete.png",:alt=>'delete',:title=>'delete')
  end
  def check_action(object)
    action = 'new'
    if object != nil
      if object[:id] != nil
        action = 'edit' 
      end
    end
    return action
  end  
  def check_id(object)    
    id = ''
    # id = object[:id] if (object[:id] != nil || object != nil)                                                                            
    if object != nil
      if object[:id] != nil
        id = object[:id]
      end
    end
    return id
  end
  def account_page                                                                
    controllers = ["account", "address_books", "measurements", "cart", "registrations"]
    valid = false   
    controllers.each do |value|
      valid = true if params[:controller] == value
    end
    # valid = true if (params[:controller] == "account" || params[:controller] == "address_books" || params[:controller] == "measurements" || params[:controller] == "registrations"                 
    return valid
  end
  
  def check_feature_valid(feature_category_name)     
    # raise session[:features_invalid].inspect
    valid = ([feature_category_name] & session[:features_invalid] == [])
    return valid
  end
  
  def get_feature(feature_info)
    # feature_category = FeatureCategory.find(feature_category_id.delete("feature_category_").to_i) 
    features = Hash.new
    feature_info.each do |k, v|
       if k != "text"                               
         value = k.camelize.constantize.find(v.to_i)
       else 
         value = OpenStruct.new
         value.name = v
         value.price = 0                          
       end
       
      features.merge!(k => value)     
    end                                                   
    return features
  end
  
  def get_category(feature_category)
    feature_category = FeatureCategory.find(feature_category.delete("feature_category_").to_i)
    return feature_category
  end 
  
  def get_user(user_id)
    user = User.find(user_id)
    return user
  end
  
  def get_cloth_type
    cloth_type = ["Shirt", "Trousers", "Jacket"]
    return cloth_type
  end
end
