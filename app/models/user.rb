class User < ActiveRecord::Base
  has_many :address_books
  has_many :measurements
  has_many :orders
  # Include default devise modules. Others available are:
  # :http_authenticatable, :token_authenticatable, :lockable, :timeoutable and :activatable
  devise :registerable, :authenticatable, :recoverable, #:confirmable, 
         :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :firstname, :lastname, :gender
  
  validates_presence_of :firstname, :lastname, :gender

  
  def name
    return firstname + " " + lastname
  end
  
  def confirmation_required?
    false
  end
end
