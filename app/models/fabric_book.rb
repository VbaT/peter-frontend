class FabricBook < ActiveRecord::Base
  has_many :fabrics
  
  def to_s
    fabric_book_code
  end
end
