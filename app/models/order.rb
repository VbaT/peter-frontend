class Order < ActiveRecord::Base
  serialize :feature_field
  serialize :measurement_field
  serialize :address_field
  belongs_to :address_book
  belongs_to :measurement


  def barcode_string
    ("%10d" % self.id)
  end

end
