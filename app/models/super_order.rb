class SuperOrder < ActiveRecord::Base
  belongs_to :user 
  serialize  :order_id_field
  serialize  :address_field 


  def orders
    self.order_id_field.map{|id| Order.find(id)}
  end

  def barcode_string
    ("%04d" % @user.id) + ("%05d" % self.id)
  end
end
