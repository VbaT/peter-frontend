class Measurement < ActiveRecord::Base
  belongs_to :user
  has_many :orders  
  
  def get_measurement_name
    return name
  end
end
