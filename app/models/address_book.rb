class AddressBook < ActiveRecord::Base
  belongs_to :user
  has_many :orders  
  validates_presence_of :address, :city, :country, :postcode
  
  def get_address_name
     return name
  end
end
