class Fabric < ActiveRecord::Base
  belongs_to :style
  belongs_to :color
  belongs_to :fabric_book   
  belongs_to :fabric_type
  has_attached_file :photo,
    :styles => {
      :big  => "512x512>",
      :small  => "256x256>",
      :thumb=> "136x136#",
      :small_thumb  => "48x48#"}
end