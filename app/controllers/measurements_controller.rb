class MeasurementsController < ApplicationController
  # before_filter :authenticate_user!     
  before_filter :get_needed_var, :only => [:index, :new, :edit, :create]
  # GET /measurements
  # GET /measurements.xml
  layout "main_layout.html.haml"
  def index
    # @measurements = Measurement.all
    @measurements = Measurement.find(:all, :conditions => "user_id =#{current_user.id}")
   
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @measurements }
    end
  end

  # GET /measurements/1
  # GET /measurements/1.xml
  def show
    @measurement = Measurement.find(params[:id])
    #session[:measurement_info] = '1234'
    #session[:measurement_info] =  @measurement.id   # show id

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @measurement }
    end
  end

  # GET /measurements/new
  # GET /measurements/new.xml
  def new
    @measurement = Measurement.new()
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @measurement }
    end 
  end


  def measurement_info
    @measurement = Measurement.new(params[:measurement])
    session[:measurement_info] = params[:measurement] 
  end


  # GET /measurements/1/edit
  def edit
    @measurement = Measurement.find(params[:id])             
    # @address_book = AddressBook.first(:conditions => "user_id =#{current_user.id}")
    # @measurement = Measurement.first(:conditions => "user_id =#{current_user.id}")
  end

  # POST /measurements
  # POST /measurements.xml

#-----------------------------------------------------------------------------------

  def create               
    @measurement = Measurement.new(params[:measurement])
    @measurement.update_attributes(:user_id => current_user.id) # assign current user id to address book
    @measurement.save    
#    session[:measurement_info] = params[:measurement]    # <----// retrieve all column
#      redirect_to "/wizard"

    respond_to do |format|
      if @measurement.save
        format.html { redirect_to(:controller => 'account', :notice => 'Measurement was successfully created.') }
        format.xml  { render :xml => @measurement, :status => :created, :location => @measurement }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @measurement.errors, :status => :unprocessable_entity }
      end
    end
      
  end

  # PUT /measurements/1
  # PUT /measurements/1.xml
  def update         
    @measurement = Measurement.first(:conditions => "user_id =#{current_user.id}")
    respond_to do |format|
      if @measurement.update_attributes(params[:measurement])
        format.html { redirect_to(:controller => 'account', :notice => 'Measurement was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @measurement.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /measurements/1
  # DELETE /measurements/1.xml
  def destroy
    @measurement = Measurement.find(params[:id])
    @measurement.destroy

    respond_to do |format|
      format.html { redirect_to(measurements_url) }
      format.xml  { head :ok }
    end
  end
end
