# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  # protect_from_forgery # See ActionController::RequestForgeryProtection for details

  layout :layout

  private

  def layout
    # only turn it off for login pages:
    devise_controller? ? false : "application"
    # or turn layout off for every devise controller:
#    devise_controller? && "application
  end
  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
  def get_needed_var    
    if current_user.class.to_s == "User"    
      # @address_book = AddressBook.first(:all, :conditions => "user_id =#{current_user.id}")
      # @measurement = Measurement.first(:all, :conditions => "user_id =#{current_user.id}")
      # @address_book = AddressBook.all
      @address_book = AddressBook.find(:all, :conditions => "user_id =#{current_user.id}")
      @measurement = Measurement.find(:all, :conditions => "user_id =#{current_user.id}")      
    end       
  end
  
  def get_show_order_var
    @order = Order.find(params[:id])
    @fabric = Fabric.find(@order.fabric_id)
    @measurement = @order.measurement_field         
    @features = @order.feature_field
    @quantity = @order.volume
    @total_price = @order.price                  
  end
  def get_price(feature_info)
    price = 0
    feature_info.each do |k, v|
       if k != "text"                               
         value = k.camelize.constantize.find(v.to_i)
         price += value.price.to_i
       else 
         price += 0
       end
    end                                                   
    return price
  end
  
end
