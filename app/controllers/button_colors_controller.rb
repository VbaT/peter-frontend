class ButtonColorsController < ApplicationController

  before_filter :find_button_color

  BUTTON_COLORS_PER_PAGE = 20

  def create
    @button_color = ButtonColor.new(params[:button_color])
    respond_to do |format|
      if @button_color.save
        flash[:notice] = 'ButtonColor was successfully created.'
        format.html { redirect_to @button_color }
        format.xml  { render :xml => @button_color, :status => :created, :location => @button_color }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @button_color.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @button_color.destroy
        flash[:notice] = 'ButtonColor was successfully destroyed.'        
        format.html { redirect_to button_colors_path }
        format.xml  { head :ok }
      else
        flash[:error] = 'ButtonColor could not be destroyed.'
        format.html { redirect_to @button_color }
        format.xml  { head :unprocessable_entity }
      end
    end
  end

  def index
    @button_colors = ButtonColor.paginate(:page => params[:page], :per_page => BUTTON_COLORS_PER_PAGE)
    respond_to do |format|
      format.html
      format.xml  { render :xml => @button_colors }
    end
  end

  def edit
  end

  def new
    @button_color = ButtonColor.new
    respond_to do |format|
      format.html
      format.xml  { render :xml => @button_color }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.xml  { render :xml => @button_color }
    end
  end

  def update
    respond_to do |format|
      if @button_color.update_attributes(params[:button_color])
        flash[:notice] = 'ButtonColor was successfully updated.'
        format.html { redirect_to @button_color }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @button_color.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def find_button_color
    @button_color = ButtonColor.find(params[:id]) if params[:id]
  end

end