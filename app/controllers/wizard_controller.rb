class WizardController < ApplicationController
  before_filter :authenticate_user!, :only => :confirm
  before_filter :check_hash, :only => [:confirm]
  before_filter :check_shop, :only => [:choose_fabric]
  # before_filter :check_state_validation,  :only => [:choose_fabric, :choose_feature, :choose_measurement, :summary]
  layout "main_layout"
  $no_monogram = "no monogram"
  $no_contrast = "no contrast"
  $shopping_hash = [:features_invalid, :fabric_info, :fabric_page, :measurement_info, :measurement_id, :summary_confirmed, :features_info, :cloth_type]
  $CONTRAST = "Contrast"
  $MONOGRAM = "Monogram"
  $BUTTON = "Button"

  def index
    # reset_session
    render :layout => "main_layout.html.haml"
  end
  
  def check_shop
    @cloth_type = params[:cloth]
    if !session.has_key?(:cloth_type)      
      session[:cloth_type] = @cloth_type
    end
  end
  
  def choose_fabric
    @fabrics_saved = false
    
    if session[:fabric_info]
      @fabrics_saved = true
      @fabric = Fabric.find(session[:fabric_info])
      @data = Fabric.all(:conditions=>["color_id = ? AND style_id = ?", @fabric.color_id, @fabric.style_id])
    end

    if @fabrics_saved
      @colors = Color.all(:conditions=>["name != ?", @fabric.color.name])
      @styles = Style.all(:conditions=>["name != ?", @fabric.style.name])
      @colors.unshift(Color.find(@fabric.color_id))
      @styles.unshift(Style.find(@fabric.style_id))
      @page = session[:fabric_page]
      #render :partial => "get_fabrics_partial",:object => @data
    else
      @colors = Color.all
      @styles = Style.all
    end
    #render :layout => 'main_layout.html.haml'
    #render :text => "Get fabric"
    #render :template => "wizard/get_fabrics"
    #render :action => "get_saved_fabrics", :layout => true
    #@fabrics = Fabric.all
  end

  def save_fabric
    session[:fabric_info] = params['fabric']
    session[:fabric_page] = params['fabric_page']
    render :text => ""     
  end

  def save_feature
    @values = Hash.new 
    params.each do |k,v|
      if k =~ /^feature_category/
        parent_key = k.gsub(/\D/, "")
        key = v.gsub(/_[0-9]+$/, "")
        value = v.gsub(/([a-z]|_)+/, "")
        if k =~ /^feature_category_text_monogram_/
          if v != ""                                   
            @values["feature_category_#{parent_key}"].merge!(Hash["text",v])
          end
          next
        end
        @values["feature_category_#{parent_key}"] = {} if @values["feature_category_#{parent_key}"] == nil
        @values["feature_category_#{parent_key}"].merge!(Hash[key,value])
      end
    end
    # raise @values.inspect
    session[:features_info] = @values
    render :text => ""
  end

  def choose_feature          
    @feature_categories = FeatureCategory.all
    @button_colors = ButtonColor.all    
    @thread_colors = ThreadColor.all
    @monogram_locations = MonogramLocation.all
    @contrast_locations = ContrastLocation.all
    @fabrics = ContrastFabric.all 
    @no_monogram = "no monogram"
    # raise session.inspect
    if session[:features_invalid] != nil    
      if session[:features_invalid].empty?() 
        session[:features_invalid] = Array.new         
        @feature_categories.each do |feature_name|       
          session[:features_invalid] << feature_name.name
        end                                         
      end
    end
    if session[:features_info]
      @features = session[:features_info]
      @has_features = true
    else 
      session[:features_info] = Hash.new
      @features = session[:features_info]
    end           
    render :layout => 'main_layout.html.haml'
  end

  def get_fabrics
    @page = params["page"].to_i
    @page = 1 if @page == 0
    if params["action"] == "next"
      @page += 1
    end
    # session[:fabric_page] = @page
    if session[:fabric_info] #check saved fabric
      @fabric = Fabric.find(session[:fabric_info])
    end
    @fabrics = Fabric.paginate(:per_page => 15,
                            :page => @page.to_i,
                            :order => 'name',
                            :conditions => ["color_id = ? AND style_id = ?", params['color']["name"], params['style']["name"]]
                            )
    #    session[:fabric_info] = @data.ideve
    #    session[:fabric_info] = @fabric_info
    #   @fabric_info = @data.find(params[:id])
    #    session[:fabric_info] =@fabric_info.id
    #render :text => "Get fabric" }
    render :partial => 'fabrics.html.haml' #route to _get_fabrics.html.haml instead
  end
  
  def order # get features to params in session
    @values = Hash.new
    params.each do |k,v|
      if k =~ /^feature_category/
        @values[k] = v
      end
    end
  end      

  def choose_measurement
    # USE ONLY IF EXISTS
    if session[:measurement_info]
      @measurement = Measurement.new session[:measurement_info]
    else
      @measurement = Measurement.new
      session[:quantity] = 1
    end
    
    if user_signed_in?
      @created_measurement = Measurement.find(:all, :conditions => "user_id = #{current_user.id}")
    end
    
    @quantity = session[:quantity]
  end
  
  def get_measurement
     measurement_id = params["measurement_id"]
     measurement_info = Measurement.find(measurement_id)          
     measurement_info.attributes.each do |key, value|
       if value.class.to_s == "String"
         measurement_info[key] = value.underscore
       end                                              
     end
     render :json => measurement_info.to_json
  end

  def save_measurement       
    session[:measurement_info] = params[:measurement]
    session[:quantity] = params["quantity"]     
    # raise session[:measurement_info].inspect
    # redirect_to :action => "summary"
    render :text => ""    
  end
  
  def save_address       
    session[:address_id] = params[:address_id]
    @address_info = AddressBook.find(session[:address_id])
    # redirect_to :action => "summary"
    render :json => @address_info
  end

  def summary   
    @measurement = Measurement.new(params[:measurement])
    @quantity = session[:quantity]
  end

  def save_summary
    session[:summary_confirmed] = true
    session[:quantity] = params["quantity"]
    render :text => ""
  end

  def save_quantity
    session[:quantity] = params["quantity"]   
    render :text => ""
  end                 
  
  
  def confirm #authen first                        
    @created_address_book = AddressBook.all(:conditions => ["user_id =#{current_user.id}"])
    @measurement = Measurement.new(session[:measurement_info])
    @user = User.find(current_user.id)
    @quantity = session[:quantity]
    
    
    if session[:address_id]
      @address_book = AddressBook.find(session[:address_id])
    else
      @address_book = AddressBook.last(:conditions => ["user_id =#{current_user.id}"])
      session[:address_id] = @address_book.id
    end
    
    if params["order_completed"] == "1"
      unit_price = 0
      total_price = 0        
      address = AddressBook.find(session[:address_id])
      address = address.attributes
      session[:features_info].each do |feature_category_id, feature_info|
        price = get_price(feature_info)
        unit_price += price
      end
      total_price = unit_price * session[:quantity].to_i
      order_name = params["post"]["order_name"]
      order_hash = Hash["fabric_id"      => session[:fabric_info],
                        "feature_field"  => session[:features_info],
                        "measurement_field" => session[:measurement_info],
                        "price" => total_price,
                        "quantity" => session[:quantity], 
                        "cloth_type" => session[:cloth_type],
                        "order_name" => order_name,
                        "flag" => false]
      session[:super_order] << order_hash
      redirect_to "/wizard/order_complete"
      return
    end
  end
  
  def order_complete
    $shopping_hash.each{|key| session.delete key}
  end
  
  def delete_monogram_info 
    key = params["name"]                           
    monogram = session[:features_info][key]
    if monogram != nil
      monogram.each {|key, value| monogram.delete key}
    end
    # monogram.delete("monogram_location") if monogram.key?("monogram_location")
    # monogram.delete("thread") if monogram.key?("thread")
    # monogram.delete("text") if monogram.key?("text")
    render :text => ""
  end
  
  def delete_contrast_info
    key = params["name"]
    contrast = session[:features_info][key]
    if contrast != nil
      contrast.each {|key, value| contrast.delete key}
    end
    # contrast.delete("contrast_location") if contrast.key?("contrast_location")
    # contrast.delete("fabric") if contrast.key?("fabric")                      
    render :text => ""
  end
  
  def states                  
    validation = {"fabric" => false, "feature" => false, "measurement" => false, "summary" => false, "confirm" => false}
    clickable  = {"fabric" => false, "feature" => false, "measurement" => false, "summary" => false, "confirm" => false} 
    session[:features_invalid] = Array.new
    validation["fabric"] = true if (session[:fabric_info] != nil)    
    if ((session[:features_info] != nil) && (!session[:features_info].empty?))    
      validation["feature"] = true   
      FeatureCategory.all.each do |feature_category|
        feature_validation = session[:features_info].key?("feature_category_" + feature_category.id.to_s)
        if feature_validation
          feature_validation = feature_validation && session[:features_info]["feature_category_" + feature_category.id.to_s].key?("feature")
        end
        validation["feature"] = validation["feature"] && feature_validation
        set_feature_invalid(feature_validation, feature_category.name)
      end                     
      # Button Category validation
      button = get_info($BUTTON)
      if button != nil
        feature_validation = button.key?("button_color") && button.key?("thread_color")
        validation["feature"] = validation["feature"] && feature_validation
        set_feature_invalid(feature_validation, $BUTTON)
      end
    
      # # Monogram Category validation                                                                      
      monogram = get_info($MONOGRAM)
      monogram_feature = Feature.find(monogram["feature"].to_i).name if monogram != nil
      if monogram_feature != $no_monogram && monogram != nil
        feature_validation = monogram.key?("monogram_location") && monogram.key?("thread_color") && monogram.key?("text")                                                                                                          
        validation["feature"] = validation["feature"] && feature_validation
        set_feature_invalid(feature_validation, $MONOGRAM)
      end
      

      # Contrast Category validation                                                                      
      contrast = get_info($CONTRAST)
      contrast_feature = Feature.find(contrast["feature"].to_i).name if contrast != nil    
      if contrast_feature != $no_contrast && contrast != nil
        feature_validation = contrast.key?("fabric") && contrast.key?("contrast_location")
        validation["feature"] = validation["feature"] && feature_validation
        set_feature_invalid(feature_validation, $CONTRAST)                
      end
    end                             
    validation["measurement"] = true if (session[:measurement_info] != nil)
    validation["summary"] = true if (session[:summary_confirmed] == true)
    
    clickable["fabric"] = true
    clickable["feature"] = validation["fabric"]
    clickable["measurement"] = clickable["feature"] && validation["feature"]
    clickable["summary"] = clickable["measurement"] && validation["measurement"]
    clickable["confirm"] = validation["summary"] && clickable["summary"]
    clickable["invalid"] = session[:features_invalid]
    render :json => clickable.to_json
  end
  
  def clear_session
    $shopping_hash.each{|key| session.delete key}    
    render :text => ""
  end   
  
  private
  
  def set_feature_invalid(validation, feature_category_id)
    if !validation
      session[:features_invalid] << feature_category_id
    end
  end
  
  def get_info(name)            
    name_id = FeatureCategory.find(:first, :conditions => ["name = ?", name]).id.to_s
    name_info = session[:features_info]["feature_category_" + name_id]    
    return name_info
  end 
  
  def check_hash     
    no_confirm_hashed = true
    $shopping_hash.each do |key| 
      no_confirm_hashed = no_confirm_hashed && !session.has_key?(key)
    end                                                 
    redirect_to :action => "choose_fabric" if no_confirm_hashed
  end
end