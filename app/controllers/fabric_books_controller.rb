class FabricBooksController < ApplicationController

  before_filter :find_fabric_book

  FABRIC_BOOKS_PER_PAGE = 20

  def create
    @fabric_book = FabricBook.new(params[:fabric_book])
    respond_to do |format|
      if @fabric_book.save
        flash[:notice] = 'FabricBook was successfully created.'
        format.html { redirect_to @fabric_book }
        format.xml  { render :xml => @fabric_book, :status => :created, :location => @fabric_book }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @fabric_book.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @fabric_book.destroy
        flash[:notice] = 'FabricBook was successfully destroyed.'        
        format.html { redirect_to fabric_books_path }
        format.xml  { head :ok }
      else
        flash[:error] = 'FabricBook could not be destroyed.'
        format.html { redirect_to @fabric_book }
        format.xml  { head :unprocessable_entity }
      end
    end
  end

  def index
    @fabric_books = FabricBook.paginate(:page => params[:page], :per_page => FABRIC_BOOKS_PER_PAGE)
    respond_to do |format|
      format.html
      format.xml  { render :xml => @fabric_books }
    end
  end

  def edit
  end

  def new
    @fabric_book = FabricBook.new
    respond_to do |format|
      format.html
      format.xml  { render :xml => @fabric_book }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.xml  { render :xml => @fabric_book }
    end
  end

  def update
    respond_to do |format|
      if @fabric_book.update_attributes(params[:fabric_book])
        flash[:notice] = 'FabricBook was successfully updated.'
        format.html { redirect_to @fabric_book }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @fabric_book.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def find_fabric_book
    @fabric_book = FabricBook.find(params[:id]) if params[:id]
  end

end