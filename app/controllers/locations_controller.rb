class LocationsController < ApplicationController

  before_filter :find_location

  LOCATIONS_PER_PAGE = 20

  def create
    @location = params[:location]["type"].constantize.new(params[:location])
    respond_to do |format|
      if @location.save   
        flash[:notice] = 'Location was successfully created.'
        format.html { redirect_to location_path(@location) }
        format.xml  { render :xml => @location, :status => :created, :location => @location }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @location.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @location.destroy
        flash[:notice] = 'Location was successfully destroyed.'        
        format.html { redirect_to locations_path }
        format.xml  { head :ok }
      else
        flash[:error] = 'Location could not be destroyed.'
        format.html { redirect_to @location }
        format.xml  { head :unprocessable_entity }
      end
    end
  end

  def index
    @locations = Location.paginate(:page => params[:page], :per_page => LOCATIONS_PER_PAGE)
    respond_to do |format|
      format.html
      format.xml  { render :xml => @locations }
    end
  end

  def edit
  end

  def new
    @location = Location.new
    respond_to do |format|
      format.html
      format.xml  { render :xml => @location }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.xml  { render :xml => @location }
    end
  end

  def update
    respond_to do |format|
      @location = params[:location]["type"].constantize.new(params[:location])
      if @location.update_attributes(params[:location])
        flash[:notice] = 'Location was successfully updated.'
        format.html { redirect_to location_path(@location) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @location.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def find_location
    @location = Location.find(params[:id]) if params[:id]
  end

end