class AccountController < ApplicationController
  before_filter :authenticate_user!   
  before_filter :get_needed_var, :only => [:index]       
  layout "main_layout"                    
  def index

  end

  def address 
    # User address_books instead     
    @address_books = AddressBook.find(:all, :conditions => "user_id =#{current_user.id}")
    @address_book = AddressBook.new
  end

  def measurement
    @measurements = Measurement.find(:all, :conditions => "user_id =#{current_user.id}")
  end

  def show_order
    # @orders = Order.find(:all, :conditions => "user_id =#{current_user.id}")
    @orders = SuperOrder.find(:all, :conditions => "user_id =#{current_user.id}")
  end
  
  def show_sub_order
    @super_order = SuperOrder.find(params[:id])
    @orders = Array.new
    orders_id = @super_order.order_id_field
    orders_id.each do |item|
       @orders << Order.find(item)
    end
  end 
  
  def show_item
    @order = Order.find(params[:id])
    @measurement = @order["measurement_field"]
    @features = @order.feature_field
    @quantity = @order.volume
    @total_price = @order.price
    @order_id = params[:id]
    get_show_order_var
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @order }
    end       
  end

  def measurement_new    
    #@measurement = Measurement.new(session[:measurement_info])
    @measurement = Measurement.new

    #    session[:measurement_info] = params[:measurement]    # <----// retrieve all column
    #      redirect_to "/wizard"

    #    respond_to do |format|
    #      if @measurement.save
    #        format.html { redirect_to(@measurement, :notice => 'Measurement was successfully created.') }
    #        format.xml  { render :xml => @measurement, :status => :created, :location => @measurement }
    #      else
    #        format.html { render :action => "new" }
    #        format.xml  { render :xml => @measurement.errors, :status => :unprocessable_entity }
    #      end
    #    end
  end

  def measurement_create 
    # @measurement = Measurement.first(:conditions => "user_id =#{current_user.id}")
    # @measurement = Measurement.new(params[:measurement])
    # @measurement = 
    @measurement = Measurement.new(params[:measurement])
    @measurement.update_attributes(:user_id => current_user.id)
    @measurement.save                   
  end

end
