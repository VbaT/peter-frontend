class CartController < ApplicationController
  before_filter :authenticate_user!   
  before_filter :get_needed_var, :only => [:index]       
  layout "main_layout"
  
  def index
    @super_order = session[:super_order]
    render :template => "cart/index.html.haml"
    
  end      
  
  def confirm_order
    order_id = Array.new
    orders = Array.new     
    params["items"].each do |k, v|
      order = session[:super_order][v.to_i]                     
      @order = Order.new
      @order.update_attributes(
                              :user_id            => current_user.id,
                              :fabric_id          => order["fabric_id"],
                              :feature_field      => order["feature_field"],
                              :measurement_field  => order["measurement_field"],
                              :order_name         => order["order_name"],
                              :price              => order["price"],
                              :volume             => order["quantity"])
      @order.save       
      order_id << @order.id 
      orders << order
    end
    
    orders.each do |value|
      session[:super_order].delete value
    end                                 
    
    @super_order = SuperOrder.new
    @super_order.update_attributes(
                                  :name             => current_user.name,
                                  :user_id          => current_user.id,
                                  :order_id_field   => order_id,
                                  :address_field    => params["post"])
    @super_order.save 
    # mail_template = render_to_string :template => 'wizard/mail_template.html.erb', :layout=>false
    # 
    # mail_success = Pony.mail(
    # :to        => @user.email,
    # :from      => 'peter.srikuruwarn@tailormaker.com',
    # :bcc       => "peter.srikuruwarn@tailormaker.com",
    # :subject   => 'Your order from Tailormaker', 
    # :html_body => mail_template,
    # :via       => :smtp,
    # :via_options => {
    #   :address              => 'smtp.gmail.com',
    #   :port                 => '587',
    #   :enable_starttls_auto => true,
    #   :user_name            => 'peter.srikuruwarn@tailormaker.com',
    #   :password             => '342342342',
    #   :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
    #   :domain               => "tailormaker.com" # the HELO domain provided by the client to the server
    # })
    redirect_to :action => "order_complete"
  end   
  
  def order_complete
    
  end
  
  def billing_form
    @created_address_book = AddressBook.all(:conditions => ["user_id =#{current_user.id}"])
    @orders = Array.new    
    @super_order = session[:super_order]
    params.each do |k, v| 
      if k =~ /^item/
        @orders << v.to_i
        # @orders << session[:super_order][v.to_i]
      end
    end
    
  end
  
  def show
    index = params["index"]
    order = session[:super_order][index.to_i]

    @features = order["feature_field"]
    @fabric = Fabric.find(order["fabric_id"])
    @measurement = order["measurement_field"]
    @quantity = order["quantity"]
    @total_price = order["price"]

  end
  
  def delete_item
    params.each do |k, v|
      if k =~ /^item/
        session[:super_order].delete_at(v.to_i)
      end
    end
    render :text => ""
  end

end
