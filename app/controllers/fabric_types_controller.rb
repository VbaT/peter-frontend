class FabricTypesController < ApplicationController

  before_filter :find_fabric_types

  FABRIC_TYPES_PER_PAGE = 20

  def create
    @fabric_types = FabricTypes.new(params[:fabric_types])
    respond_to do |format|
      if @fabric_types.save
        flash[:notice] = 'FabricTypes was successfully created.'
        format.html { redirect_to @fabric_types }
        format.xml  { render :xml => @fabric_types, :status => :created, :location => @fabric_types }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @fabric_types.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @fabric_types.destroy
        flash[:notice] = 'FabricTypes was successfully destroyed.'        
        format.html { redirect_to fabric_types_path }
        format.xml  { head :ok }
      else
        flash[:error] = 'FabricTypes could not be destroyed.'
        format.html { redirect_to @fabric_types }
        format.xml  { head :unprocessable_entity }
      end
    end
  end

  def index
    @fabric_types = FabricTypes.paginate(:page => params[:page], :per_page => FABRIC_TYPES_PER_PAGE)
    respond_to do |format|
      format.html
      format.xml  { render :xml => @fabric_types }
    end
  end

  def edit
  end

  def new
    @fabric_types = FabricTypes.new
    respond_to do |format|
      format.html
      format.xml  { render :xml => @fabric_types }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.xml  { render :xml => @fabric_types }
    end
  end

  def update
    respond_to do |format|
      if @fabric_types.update_attributes(params[:fabric_types])
        flash[:notice] = 'FabricTypes was successfully updated.'
        format.html { redirect_to @fabric_types }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @fabric_types.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def find_fabric_types
    @fabric_types = FabricTypes.find(params[:id]) if params[:id]
  end

end