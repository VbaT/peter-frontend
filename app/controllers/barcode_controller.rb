class BarcodeController < ApplicationController
  require 'barby'
  require 'barby/outputter/rmagick_outputter'
  require 'barby/barcode/code_128'
  def get_barcode_image
    string_to_encode = params[:id]

    barcode = Barby::Code128B.new(string_to_encode.upcase)
    render :text=>barcode.to_png, :content_type => "image/png"
  end
end