class OrdersController < ApplicationController
  # before_filter :authenticate_admin!  
  before_filter :get_needed_var, :only => [:index, :new, :edit, :create]
  before_filter :check_layout, :only => [:index, :new, :edit, :create]
  
                
  # layout "main_layout.html"
  # GET /orders
  # GET /orders.xml
  def index
    # @orders = Order.all
    @super_orders = SuperOrder.all
    respond_to do |format|
      format.html { render :layout => @layout }# index.html.erb
      format.xml  { render :xml => @orders }
    end    
  end

  # GET /orders/1
  # GET /orders/1.xml
  def show
    get_show_order_var

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @order }
    end
  end
  
  # def show_order
  #   @super_order = SuperOrder.find(params["id"])
  #   @orders = Array.new        
  #   @super_order.order_id_field.each do |order_id|
  #     @orders << Order.find(order_id)
  #   end 
  #   @user = User.find(@super_order.user_id)
  # end


  def show_order
    @super_order = SuperOrder.find(params["id"])
    @orders = @super_order.orders
    @user = @super_order.user
  end
  
  def print_super_order
    @super_order = SuperOrder.find(params["id"])
    @orders = @super_order.orders
    @user = @super_order.user
    # render :template => "orders/show_order", :layout => "print"
    # return
    summary_output = render_to_string :template => "orders/show_order", :layout => "print"
    save_pdf("super_order_#{@super_order.id}", summary_output)
    file_list = [pdf_path("super_order_#{@super_order.id}")]
    @orders.each do |order|
      
      @order = order
      @fabric = Fabric.find(@order.fabric_id)
      @measurement = @order.measurement_field         
      @features = @order.feature_field
      @quantity = @order.volume
      @total_price = @order.price


      data = render_to_string :template => "orders/show", :layout => "print"
      save_pdf("order_#{order.id}", data)
      file_list << pdf_path("order_#{order.id}")
    end
    system "pdftk #{file_list.join(' ')} output #{pdf_path('combined_' + @super_order.id.to_s)}"
    send_data(File.read(pdf_path("combined_#{@super_order.id}")),
      :filename => "output.pdf",
      :type => 'application/pdf'
    )
    
    # end
    # pdf = PDFKit.new(output)
    # pdf.stylesheets << RAILS_ROOT + '/public/stylesheets/print.css'
    # save_pdf("order", pdf.to_pdf)
  end
  
  def print_order
    @order = Order.find(params["id"])
    get_show_order_var
    output = render_to_string :template => "orders/show", :layout => "print"
    send_data(File.read(output),
      :filename => "output.pdf",
      :type => 'application/pdf'
    ) 
  end
  
  # GET /orders/new
  # GET /orders/new.xml
  def new
    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.xml
  def create
    @order = Order.new(params[:order])

    respond_to do |format|
      if @order.save
        format.html { redirect_to(@order, :notice => 'Order was successfully created.') }
        format.xml  { render :xml => @order, :status => :created, :location => @order }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.xml
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to(@order, :notice => 'Order was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.xml
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to(orders_url) }
      format.xml  { head :ok }
    end
  end
  
  def check_layout   
    @layout = true          
    # if current_user.class.to_s == "User"
    #   @layout = "main_layout.html.haml"
    # else
    #   @layout = true
    # end                   
  end
  
  private 
  
  def save_pdf(file_name, html)
    path = pdf_path(file_name)
    pdf = PDFKit.new(html)
    pdf.stylesheets << "#{RAILS_ROOT}/public/stylesheets/narrow.css"
    pdf.stylesheets << "#{RAILS_ROOT}/public/stylesheets/print.css"
    pdf.to_file(path)
  end
  
  def pdf_path(file_name)
    path = "#{RAILS_ROOT}/tmp/pdfs/#{file_name}.pdf"
  end
  
end
