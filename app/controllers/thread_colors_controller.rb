class ThreadColorsController < ApplicationController

  before_filter :find_thread_color

  THREAD_COLORS_PER_PAGE = 20

  def create
    @thread_color = ThreadColor.new(params[:thread_color])
    respond_to do |format|
      if @thread_color.save
        flash[:notice] = 'ThreadColor was successfully created.'
        format.html { redirect_to @thread_color }
        format.xml  { render :xml => @thread_color, :status => :created, :location => @thread_color }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @thread_color.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @thread_color.destroy
        flash[:notice] = 'ThreadColor was successfully destroyed.'        
        format.html { redirect_to thread_colors_path }
        format.xml  { head :ok }
      else
        flash[:error] = 'ThreadColor could not be destroyed.'
        format.html { redirect_to @thread_color }
        format.xml  { head :unprocessable_entity }
      end
    end
  end

  def index              
    @thread_colors = ThreadColor.paginate(:page => params[:page], :per_page => THREAD_COLORS_PER_PAGE)
    respond_to do |format|
      format.html
      format.xml  { render :xml => @thread_colors }
    end
  end

  def edit
  end

  def new
    @thread_color = ThreadColor.new
    respond_to do |format|
      format.html
      format.xml  { render :xml => @thread_color }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.xml  { render :xml => @thread_color }
    end
  end

  def update
    respond_to do |format|
      if @thread_color.update_attributes(params[:thread_color])
        flash[:notice] = 'ThreadColor was successfully updated.'
        format.html { redirect_to @thread_color }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @thread_color.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def find_thread_color
    @thread_color = ThreadColor.find(params[:id]) if params[:id]
  end

end