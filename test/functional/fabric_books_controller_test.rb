require 'test_helper'

class FabricBooksControllerTest < ActionController::TestCase

  test "should create fabric_book" do
    FabricBook.any_instance.expects(:save).returns(true)
    post :create, :fabric_book => { }
    assert_response :redirect
  end

  test "should handle failure to create fabric_book" do
    FabricBook.any_instance.expects(:save).returns(false)
    post :create, :fabric_book => { }
    assert_template "new"
  end

  test "should destroy fabric_book" do
    FabricBook.any_instance.expects(:destroy).returns(true)
    delete :destroy, :id => fabric_books(:one).to_param
    assert_not_nil flash[:notice]    
    assert_response :redirect
  end

  test "should handle failure to destroy fabric_book" do
    FabricBook.any_instance.expects(:destroy).returns(false)    
    delete :destroy, :id => fabric_books(:one).to_param
    assert_not_nil flash[:error]
    assert_response :redirect
  end

  test "should get edit for fabric_book" do
    get :edit, :id => fabric_books(:one).to_param
    assert_response :success
  end

  test "should get index for fabric_books" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fabric_books)
  end

  test "should get new for fabric_book" do
    get :new
    assert_response :success
  end

  test "should get show for fabric_book" do
    get :show, :id => fabric_books(:one).to_param
    assert_response :success
  end

  test "should update fabric_book" do
    FabricBook.any_instance.expects(:save).returns(true)
    put :update, :id => fabric_books(:one).to_param, :fabric_book => { }
    assert_response :redirect
  end

  test "should handle failure to update fabric_book" do
    FabricBook.any_instance.expects(:save).returns(false)
    put :update, :id => fabric_books(:one).to_param, :fabric_book => { }
    assert_template "edit"
  end

end