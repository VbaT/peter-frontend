require 'test_helper'

class ThreadColorsControllerTest < ActionController::TestCase

  test "should create thread_color" do
    ThreadColor.any_instance.expects(:save).returns(true)
    post :create, :thread_color => { }
    assert_response :redirect
  end

  test "should handle failure to create thread_color" do
    ThreadColor.any_instance.expects(:save).returns(false)
    post :create, :thread_color => { }
    assert_template "new"
  end

  test "should destroy thread_color" do
    ThreadColor.any_instance.expects(:destroy).returns(true)
    delete :destroy, :id => thread_colors(:one).to_param
    assert_not_nil flash[:notice]    
    assert_response :redirect
  end

  test "should handle failure to destroy thread_color" do
    ThreadColor.any_instance.expects(:destroy).returns(false)    
    delete :destroy, :id => thread_colors(:one).to_param
    assert_not_nil flash[:error]
    assert_response :redirect
  end

  test "should get edit for thread_color" do
    get :edit, :id => thread_colors(:one).to_param
    assert_response :success
  end

  test "should get index for thread_colors" do
    get :index
    assert_response :success
    assert_not_nil assigns(:thread_colors)
  end

  test "should get new for thread_color" do
    get :new
    assert_response :success
  end

  test "should get show for thread_color" do
    get :show, :id => thread_colors(:one).to_param
    assert_response :success
  end

  test "should update thread_color" do
    ThreadColor.any_instance.expects(:save).returns(true)
    put :update, :id => thread_colors(:one).to_param, :thread_color => { }
    assert_response :redirect
  end

  test "should handle failure to update thread_color" do
    ThreadColor.any_instance.expects(:save).returns(false)
    put :update, :id => thread_colors(:one).to_param, :thread_color => { }
    assert_template "edit"
  end

end