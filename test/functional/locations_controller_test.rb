require 'test_helper'

class LocationsControllerTest < ActionController::TestCase

  test "should create location" do
    Location.any_instance.expects(:save).returns(true)
    post :create, :location => { }
    assert_response :redirect
  end

  test "should handle failure to create location" do
    Location.any_instance.expects(:save).returns(false)
    post :create, :location => { }
    assert_template "new"
  end

  test "should destroy location" do
    Location.any_instance.expects(:destroy).returns(true)
    delete :destroy, :id => locations(:one).to_param
    assert_not_nil flash[:notice]    
    assert_response :redirect
  end

  test "should handle failure to destroy location" do
    Location.any_instance.expects(:destroy).returns(false)    
    delete :destroy, :id => locations(:one).to_param
    assert_not_nil flash[:error]
    assert_response :redirect
  end

  test "should get edit for location" do
    get :edit, :id => locations(:one).to_param
    assert_response :success
  end

  test "should get index for locations" do
    get :index
    assert_response :success
    assert_not_nil assigns(:locations)
  end

  test "should get new for location" do
    get :new
    assert_response :success
  end

  test "should get show for location" do
    get :show, :id => locations(:one).to_param
    assert_response :success
  end

  test "should update location" do
    Location.any_instance.expects(:save).returns(true)
    put :update, :id => locations(:one).to_param, :location => { }
    assert_response :redirect
  end

  test "should handle failure to update location" do
    Location.any_instance.expects(:save).returns(false)
    put :update, :id => locations(:one).to_param, :location => { }
    assert_template "edit"
  end

end