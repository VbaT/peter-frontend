require 'test_helper'

class FabricTypesControllerTest < ActionController::TestCase

  test "should create fabric_types" do
    FabricTypes.any_instance.expects(:save).returns(true)
    post :create, :fabric_types => { }
    assert_response :redirect
  end

  test "should handle failure to create fabric_types" do
    FabricTypes.any_instance.expects(:save).returns(false)
    post :create, :fabric_types => { }
    assert_template "new"
  end

  test "should destroy fabric_types" do
    FabricTypes.any_instance.expects(:destroy).returns(true)
    delete :destroy, :id => fabric_types(:one).to_param
    assert_not_nil flash[:notice]    
    assert_response :redirect
  end

  test "should handle failure to destroy fabric_types" do
    FabricTypes.any_instance.expects(:destroy).returns(false)    
    delete :destroy, :id => fabric_types(:one).to_param
    assert_not_nil flash[:error]
    assert_response :redirect
  end

  test "should get edit for fabric_types" do
    get :edit, :id => fabric_types(:one).to_param
    assert_response :success
  end

  test "should get index for fabric_types" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fabric_types)
  end

  test "should get new for fabric_types" do
    get :new
    assert_response :success
  end

  test "should get show for fabric_types" do
    get :show, :id => fabric_types(:one).to_param
    assert_response :success
  end

  test "should update fabric_types" do
    FabricTypes.any_instance.expects(:save).returns(true)
    put :update, :id => fabric_types(:one).to_param, :fabric_types => { }
    assert_response :redirect
  end

  test "should handle failure to update fabric_types" do
    FabricTypes.any_instance.expects(:save).returns(false)
    put :update, :id => fabric_types(:one).to_param, :fabric_types => { }
    assert_template "edit"
  end

end