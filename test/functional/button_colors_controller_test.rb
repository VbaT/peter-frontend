require 'test_helper'

class ButtonColorsControllerTest < ActionController::TestCase

  test "should create button_color" do
    ButtonColor.any_instance.expects(:save).returns(true)
    post :create, :button_color => { }
    assert_response :redirect
  end

  test "should handle failure to create button_color" do
    ButtonColor.any_instance.expects(:save).returns(false)
    post :create, :button_color => { }
    assert_template "new"
  end

  test "should destroy button_color" do
    ButtonColor.any_instance.expects(:destroy).returns(true)
    delete :destroy, :id => button_colors(:one).to_param
    assert_not_nil flash[:notice]    
    assert_response :redirect
  end

  test "should handle failure to destroy button_color" do
    ButtonColor.any_instance.expects(:destroy).returns(false)    
    delete :destroy, :id => button_colors(:one).to_param
    assert_not_nil flash[:error]
    assert_response :redirect
  end

  test "should get edit for button_color" do
    get :edit, :id => button_colors(:one).to_param
    assert_response :success
  end

  test "should get index for button_colors" do
    get :index
    assert_response :success
    assert_not_nil assigns(:button_colors)
  end

  test "should get new for button_color" do
    get :new
    assert_response :success
  end

  test "should get show for button_color" do
    get :show, :id => button_colors(:one).to_param
    assert_response :success
  end

  test "should update button_color" do
    ButtonColor.any_instance.expects(:save).returns(true)
    put :update, :id => button_colors(:one).to_param, :button_color => { }
    assert_response :redirect
  end

  test "should handle failure to update button_color" do
    ButtonColor.any_instance.expects(:save).returns(false)
    put :update, :id => button_colors(:one).to_param, :button_color => { }
    assert_template "edit"
  end

end